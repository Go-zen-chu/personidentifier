package com.go_zen_chu.personidentifier;

import android.content.Context;
import android.media.audiofx.Equalizer;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

// https://gitorious.org/0xdroid/development/source/90c1d502521db52322dc5f96973d376e8029ac0a:samples/ApiDemos/src/com/example/android/apis/media/AudioFxDemo.java を参考
public class EqualizerView extends LinearLayout {
	private LayoutInflater mLayoutInflater;
	
	public EqualizerView(Context context){
		super(context);
		initialize(context);
	}
	public EqualizerView(Context context, AttributeSet attributeSet){
		super(context, attributeSet);
		initialize(context);
	}
	
	private void initialize(Context context){
		mLayoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public void updateView(final Equalizer equalizer){
		this.removeAllViews();
		
		short bandNum = equalizer.getNumberOfBands();
		final short minEQLevel = equalizer.getBandLevelRange()[0];
		final short maxEQLevel = equalizer.getBandLevelRange()[1];
		
		for (short idx = 0; idx < bandNum; idx++) {
			View xmlLayout = mLayoutInflater.inflate(R.layout.equalizer_one_band_view, this, false);
			xmlLayout.setId(idx);
			final short band = idx;
			TextView freqTextView = (TextView)xmlLayout.findViewById(R.id.freqTextView);
	        freqTextView.setText((equalizer.getCenterFreq(band) / 1000) + " Hz");
	
	        TextView minDbTextView = (TextView)xmlLayout.findViewById(R.id.minDbTextView);
	        minDbTextView.setText((minEQLevel / 100) + " dB");
	
	        TextView maxDbTextView = (TextView)xmlLayout.findViewById(R.id.maxDbTextView);
	        maxDbTextView.setText((maxEQLevel / 100) + " dB");
		        
	        SeekBar bar = (SeekBar)xmlLayout.findViewById(R.id.progressSeekBar);
	        bar.setMax(maxEQLevel - minEQLevel);
	        bar.setProgress(equalizer.getBandLevel(band));
	        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
	            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
	            	if(equalizer != null)
	            		equalizer.setBandLevel(band, (short) (progress + minEQLevel));
	            }
	            public void onStartTrackingTouch(SeekBar seekBar) {}
	            public void onStopTrackingTouch(SeekBar seekBar) {}
	        });
	        this.addView(xmlLayout);
		}
		
	}
}
