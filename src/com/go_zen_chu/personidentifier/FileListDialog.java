package com.go_zen_chu.personidentifier;

import java.io.File;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.*;

/**
 * ファイルリストダイアログクラス
 */
public class FileListDialog extends Activity implements View.OnClickListener, DialogInterface.OnClickListener {

	private Context mParentContext = null;							//親
	private File[] mDialogFileList;						//今、表示しているファイルのリスト
	private int mSelectIdx = -1;							//選択したインデックス
	private onFileListDialogListener mListener = null;		//リスナー
	private boolean mCanSelectDir = false;			//ディレクトリ選択をするか？
	
    public void setDirectorySelect(boolean canSelectDir){
		mCanSelectDir = canSelectDir;
    }
	
    public String getSelectedFileName(){
		String ret = "";
		if(mSelectIdx >= 0){
			ret = mDialogFileList[mSelectIdx].getName();
		}
		return ret;
	}
	
    public FileListDialog(Context context){
		mParentContext = context;
	}
	

	@Override
    public void onClick(View v) {
		// 処理なし		
	}

	/**
     * ダイアログの選択イベント. 
     * <dd>メソッド名: onClick
     * <dd>メソッド説明: 
     * <dd>備考: 
     *
     * @param dialog a <code>DialogInterface</code> value
     * @param which an <code>int</code> value
     */
	@Override        
    public void onClick(DialogInterface dialog, int which) {
		//選択されたので位置を保存
		mSelectIdx = which;
		if(mDialogFileList != null && mListener != null){
			File file = mDialogFileList[which];
			
//			Util.outputDebugLog("getAbsolutePath : " + file.getAbsolutePath());
//			Util.outputDebugLog("getPath : " + file.getPath());
//			Util.outputDebugLog("getName : " + file.getName());
//			Util.outputDebugLog("getParent : " + file.getParent());
			
			if(file.isDirectory() && !mCanSelectDir){
				//選択した項目がディレクトリで、ディレクトリ選択しない場合はもう一度リスト表示
				show(file.getAbsolutePath(), file.getPath());
			}else{
				//それ以外は終了なので親のハンドラ呼び出す
				mListener.onClickFileList(file);
			}
		}
	}

	public void show(String path, String title){
		try{
			mDialogFileList = new File(path).listFiles();
			if(mDialogFileList == null){
				//NG
				if(mListener != null){
					//リスナーが登録されてたら空で呼び出す
					mListener.onClickFileList(null);
				}
			}else{
				String[] list = new String[mDialogFileList.length];
				int count = 0;
				String name = "";

				//ファイル名のリストを作る
				for (File file : mDialogFileList) {
					if(file.isDirectory()){
						//ディレクトリの場合
						name = file.getName() + "/";
					}else{
						//通常のファイル
						name = file.getName();
					}
					list[count] = name;
					count++;
				}
				//ダイアログ表示
				new AlertDialog.Builder(mParentContext).setTitle(title).setItems(list, this).show();
			}
		}catch(SecurityException se){
			//Util.outputDebugLog(se.getMessage());
		}catch(Exception e){
			//Util.outputDebugLog(e.getMessage());
		}
	}
	
	/**
     * リスナーのセット. 
     * <dd>メソッド名: setOnFileListDialogListener
     * <dd>メソッド説明: 
     * <dd>備考: 
     *
     * @param listener an <code>onFileListDialogListener</code> value
     */
    public void SetOnFileListDialogListener(onFileListDialogListener listener){
		mListener = listener;
	}
	
	/**
     * Describe interface <code>onFileListDialogListener</code> 
     * クリックイベントのインターフェースクラス.
     */
    public interface onFileListDialogListener{
		public void onClickFileList(File file);
	}
}

