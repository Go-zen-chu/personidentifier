package com.go_zen_chu.personidentifier;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import audiolib.MFCC;
import audiolib.MFCCview;
import audiolib.WaveFile;
import audiolib.WaveFile.AsyncTaskCallback;
import audiolib.WaveHeader;

public class AudioRecognitionActivity extends Activity {
	public final static String INTENT_STR_EXTRA = "wavFilePath";
	
	private final static int SAMPLE_PER_FRAME = 1024;
	private final static int CEPSTRAM_NUM = 14;
	
	private MFCCview mMFCCView;
	private String wavFilePath;
	private ArrayList<double[]> mMFCCList = new ArrayList<double[]>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.audio_recognition_activity_view);
		
		Intent intent = getIntent();
		wavFilePath = intent.getStringExtra(INTENT_STR_EXTRA);
		mMFCCView = (MFCCview)findViewById(R.id.mfccView);
		mMFCCView.setParams(CEPSTRAM_NUM, 30);
		
		final WaveFile wf = new WaveFile();
		wf.new ReadWaveFileAsync()
		.setAsyncTaskCallBack(new AsyncTaskCallback() {
			@Override
			public void onPostExecute() {
				new ProcessMFCCTask().execute(wf);
			}
		}).execute(wavFilePath);
		Button exportMfccButton = (Button)findViewById(R.id.exportMfccButton);
		exportMfccButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				new ExportMFCCTask().execute("");
			}
		});
	}
	
	private class ProcessMFCCTask extends AsyncTask<WaveFile, double[], Long> {
		@Override
		protected Long doInBackground(WaveFile... waveFiles) {
			for(WaveFile wf : waveFiles){
				WaveHeader wh = wf.getHeader();
				MFCC mfcc = new MFCC(SAMPLE_PER_FRAME, wh.getSampleRate(), CEPSTRAM_NUM);
				float[] frameArray = new float[SAMPLE_PER_FRAME];
				short[] cloneSample = wf.getSamples().clone();
				for(int frameIdx = 0; frameIdx < wh.getSamplesNum() / SAMPLE_PER_FRAME; frameIdx++){
					for(int i = 0; i < SAMPLE_PER_FRAME; i++){
						frameArray[i] = cloneSample[i + frameIdx * SAMPLE_PER_FRAME];
					}
					double[] mfccResult = mfcc.doMFCC(frameArray);
					mMFCCList.add(mfccResult);
					publishProgress(mfccResult);
				}
			}
			return null;
		}
		@Override
		protected void onProgressUpdate(double[]... values) {
			super.onProgressUpdate(values);
			mMFCCView.insertCepstraData(values[0]);
		}
	}
	private class ExportMFCCTask extends AsyncTask<String, Integer, Long> {
		@Override
		protected Long doInBackground(String... mfccFilePathes) {
			DataOutputStream dos = null;
			try {
				dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(mfccFilePathes[0])));
				for (double[] data : mMFCCList) {
					String lineStr = Double.toString(data[0]);
					for (int i = 1; i < data.length; i++) {
						lineStr += "," + data[i];
					}
					lineStr += "\n";
					dos.writeChars(lineStr);
				};
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (dos != null)
					try {
						dos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
			return null;
		}
	}

}
