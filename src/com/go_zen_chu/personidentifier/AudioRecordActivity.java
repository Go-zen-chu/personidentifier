package com.go_zen_chu.personidentifier;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import voice.TextToSpeechActivity;
import camera.FaceDetectActivity;
import camera.FaceIdentifyActivity;
import android.app.Activity;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.audiofx.Equalizer;
import android.media.audiofx.Visualizer;
import android.media.audiofx.Visualizer.OnDataCaptureListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import audiolib.WaveFile;
import audiolib.WaveHeader;

public class AudioRecordActivity extends Activity
{
    public static final String LOG_TAG = "PersonIdentifier";
    
 // Menuの設定
    private static final String MENU_AUDIO_CHANGE = "Audio change";
    private static final String MENU_MFCC = "MFCC";
    private static final String MENU_FACE = "Face Detection";
    private static final String MENU_TTS = "Text to speech";
    private static final String MENU_FIA = "Identify face";
    private HashMap<String, OnMenuItemClickListener> mMenuHashMap
    	= new HashMap<String, MenuItem.OnMenuItemClickListener>(){{
    		put(MENU_AUDIO_CHANGE, new OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					if(selectedFilePath != null && new File(selectedFilePath).exists()){
						Intent intent = new Intent(AudioRecordActivity.this, AudioRecognitionActivity.class);
						intent.putExtra(AudioRecognitionActivity.INTENT_STR_EXTRA, selectedFilePath);
						startActivity(intent);
					}
					return false;
				}
			});
    		put(MENU_MFCC, new OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					if(selectedFilePath != null && new File(selectedFilePath).exists()){
						Intent intent = new Intent(AudioRecordActivity.this, AudioRecognitionActivity.class);
						intent.putExtra(AudioRecognitionActivity.INTENT_STR_EXTRA, selectedFilePath);
						startActivity(intent);
					}
					return false;
				}
			});
    		put(MENU_FACE, new OnMenuItemClickListener() {
    			@Override
    			public boolean onMenuItemClick(MenuItem item) {
    				Intent intent = new Intent(AudioRecordActivity.this, FaceDetectActivity.class);
    				startActivity(intent);
    				return false;
    			}
    		});
    		put(MENU_TTS,  new OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					Intent intent = new Intent(AudioRecordActivity.this, TextToSpeechActivity.class);
					startActivity(intent);
					return false;
				}
			});
    		put(MENU_FIA, new OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					Intent intent = new Intent(AudioRecordActivity.this, FaceIdentifyActivity.class);
					startActivity(intent);
					return false;
				}
			});
    	}};
    
    private static final int RECORDER_SAMPLERATE = 44100;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_CONFIGURATION_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    
	public static String PROJECT_PATH = null;
    public static final String RECORD_DIR = "Record/";
    public final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmssSS");
    
    private boolean mIsPlaying;
    private boolean mIsRecording;
    private String selectedFilePath;
    // UI related vars
    private AudioRecord mAudioRec;
    private int mMinBufferSize;
    private Thread mRecordingThread;
    private Date mRecordStartTime;
    private int mRecordedBytes;
    private String mRawSaveFilePath;
    private MediaPlayer   mPlayer;
    private Visualizer mVisualizer;
    private VisualizerView mVisualizerView;
    private Equalizer mEqualizer;
    private EqualizerView mEqualizerView;
    private FileListDialog mFileListDialog;
    private FFTView mFftView;
    
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu){
    	for(String key : mMenuHashMap.keySet()){
    		MenuItem menuItem = menu.add(key);
    		menuItem.setOnMenuItemClickListener(mMenuHashMap.get(key));
    		menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
    	}
		return true;
    }
    
    public AudioRecordActivity() {
        PROJECT_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/com.go_zen_chu.personIdentifier/";
        mFileListDialog = new FileListDialog(this);
        mFileListDialog.SetOnFileListDialogListener(new FileListDialog.onFileListDialogListener() {
        	@Override
        	public void onClickFileList(File file) {
            	if(file == null){
        			Toast.makeText(AudioRecordActivity.this, "ファイルの取得ができませんでした", Toast.LENGTH_SHORT).show();
        		}else{
        			selectedFilePath = file.getAbsolutePath();
        			startPlaying(selectedFilePath);
        		}
        	}
		});
        // buffersize * 2 for smooth recording
        mMinBufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING) * 2;
    }

    
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
//		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.record_view);

        Button recordButton = (Button)findViewById(R.id.recordButton);
        recordButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Button btn = (Button)view;
                if (mIsRecording) {
                    btn.setText("Start recording");
                    stopRecording();
                } else {
                    btn.setText("Stop recording");
                    startRecording();
                }
                mIsRecording = !mIsRecording;
			}
		});
        
        Button  playButton = (Button)findViewById(R.id.playButton);
        playButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Button btn = (Button)view;
                if (mIsPlaying) {
                    btn.setText("Start playing");
                    stopPlaying();
                } else {
                    btn.setText("Stop playing");
            		mFileListDialog.show(PROJECT_PATH + RECORD_DIR, "Select audio file");
                }
                mIsPlaying = !mIsPlaying;
			}
		});
        Button gotoRecognitionButton = (Button)findViewById(R.id.gotoRecognitionButton);
        gotoRecognitionButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if(selectedFilePath != null && new File(selectedFilePath).exists()){
					Intent intent = new Intent(AudioRecordActivity.this, AudioRecognitionActivity.class);
					intent.putExtra(AudioRecognitionActivity.INTENT_STR_EXTRA, selectedFilePath);
					startActivity(intent);
				}
			}
		});
        
        mVisualizerView = (VisualizerView)findViewById(R.id.visualizerView);
        mEqualizerView = (EqualizerView)findViewById(R.id.equalizerView);
        mFftView = (FFTView)findViewById(R.id.fftView);
    }
    
    private void startRecording(){
        File recordDir = new File(PROJECT_PATH + RECORD_DIR);
        if(recordDir.exists() == false) recordDir.mkdirs();
        mAudioRec = new AudioRecord(MediaRecorder.AudioSource.MIC, RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, mMinBufferSize);
        if(mAudioRec.getState() != 1) return;
        mAudioRec.startRecording();
        mRecordingThread = new Thread(new Runnable() {
	        @Override
	        public void run() {
	        	mRecordStartTime = new Date();
	        	mRecordedBytes = 0;
	        	mRawSaveFilePath = PROJECT_PATH + RECORD_DIR + DATE_FORMAT.format(mRecordStartTime) + ".raw";
	            short[] recData = new short[mMinBufferSize];
	            DataOutputStream dos = null;
	            try {
		        	dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(mRawSaveFilePath)));
		        	int readNum = 0;
		        	while (mIsRecording) {
						readNum = mAudioRec.read(recData, 0, mMinBufferSize);
						if(readNum != AudioRecord.ERROR_BAD_VALUE && readNum != AudioRecord.ERROR_INVALID_OPERATION){
							for (int i = 0; i < readNum; i++) { // readnum分だけ書き込まないと、無駄な0が入ってしまう
								dos.writeShort(recData[i]);
							}
							mRecordedBytes += readNum * (RECORDER_AUDIO_ENCODING == AudioFormat.ENCODING_PCM_16BIT ? 2 : 1);
						}
					}
		        } catch (FileNotFoundException e) {
		        	e.printStackTrace();
		        } catch (IOException e) {
					e.printStackTrace();
				}finally{
		        	if(dos != null) try { dos.close(); } catch (IOException e) { e.printStackTrace(); }
		        }
	        }
	        
	    },"AudioRecorderThread");
	    mRecordingThread.start();
    }
	private void stopRecording() {
		if(mAudioRec == null || mRecordingThread == null) return;
		if(mAudioRec.getState() == 1) mAudioRec.stop();
		mAudioRec.release();
		mAudioRec = null;
        String wavSaveFilePath = PROJECT_PATH + RECORD_DIR + DATE_FORMAT.format(mRecordStartTime) + ".wav";
        WaveFile wf = new WaveFile();
        short numChannels = RECORDER_CHANNELS == AudioFormat.CHANNEL_IN_STEREO ? 2 : 1;
        short bitsPerSample = RECORDER_AUDIO_ENCODING == AudioFormat.ENCODING_PCM_16BIT ? 16 : 8;
    	wf.setHeader(new WaveHeader(WaveHeader.FORMAT_PCM, numChannels, RECORDER_SAMPLERATE, bitsPerSample, mRecordedBytes));
    	AsyncTask<String, Integer,Long> task = wf.new WriteWaveFileAsync(mRawSaveFilePath);
    	task.execute(wavSaveFilePath);
	}
	
    private void startPlaying(String audioFilePath){
        mPlayer = new MediaPlayer();
    	try {
            mPlayer.setDataSource(audioFilePath);
            mPlayer.prepare();
            mPlayer.start();
            // get audio session id to visualize
            int sessionId = mPlayer.getAudioSessionId();
            visualizeAudio(sessionId);
            equalizeAudio(sessionId);
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }
    private void stopPlaying(){
    	if(mPlayer == null) return;
        mPlayer.release();
        mPlayer = null;
    }
    
    private void visualizeAudio(int sessionId){
        mVisualizer = new Visualizer(sessionId);
        mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer.setDataCaptureListener(new OnDataCaptureListener() {
			@Override
			public void onWaveFormDataCapture(Visualizer visualizer, byte[] waveform, int samplingRate) {
				mVisualizerView.updateVisualizer(waveform);
			}
			@Override
			public void onFftDataCapture(Visualizer visualizer, byte[] fft, int samplingRate) {
				mFftView.setSamplingRate(samplingRate);
				mFftView.updateFft(fft);
			}
		}, Visualizer.getMaxCaptureRate(), true, true);
        mVisualizer.setEnabled(true);
    }
    private void equalizeAudio(int sessionId) {
        mEqualizer = new Equalizer(0, sessionId);
        mEqualizer.setEnabled(true);
		mEqualizerView.updateView(mEqualizer);
	}
    
    @Override
    public void onPause() {
        super.onPause();
		if (mAudioRec != null) { mAudioRec.release(); mAudioRec = null; }
        if (mPlayer != null) { mPlayer.release(); mPlayer = null; }
        if(mVisualizer != null){ mVisualizer.release(); mVisualizer = null; }
        if(mEqualizer != null) { mEqualizer.release(); mEqualizer =null;}
    }
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mAudioRec != null) { mAudioRec.release(); mAudioRec = null; }
        if (mPlayer != null) { mPlayer.release(); mPlayer = null; }
        if(mVisualizer != null){ mVisualizer.release(); mVisualizer = null; }
        if(mEqualizer != null) { mEqualizer.release(); mEqualizer =null;}
	}
    
}
