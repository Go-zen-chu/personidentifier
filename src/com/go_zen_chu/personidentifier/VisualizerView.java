package com.go_zen_chu.personidentifier;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

public class VisualizerView extends View {
    private byte[] mBytes;
    private float[] mPoints;
    private Rect mRect = new Rect();
 
    private Paint mForePaint = new Paint();
 
    public VisualizerView(Context context) {
        super(context);
        initialize();
    }
    public VisualizerView(Context context, AttributeSet attr) {
		super(context, attr);
		initialize();
	}
    
 
    private void initialize() {
        mBytes = null;
        mForePaint.setStrokeWidth(1f);
        mForePaint.setAntiAlias(true);
        mForePaint.setColor(Color.rgb(0, 128, 255));
    }
 
    public void updateVisualizer(byte[] bytes) {
        mBytes = bytes;
        invalidate();
    }
 
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
 
        if (mBytes == null) {
            return;
        }
        int byteFrameLength = mBytes.length;
        if (mPoints == null || mPoints.length < byteFrameLength * 4) {
            mPoints = new float[byteFrameLength * 4];
        }
        mRect.set(0, 0, getWidth(), getHeight());
        int rectHalfHeight = mRect.height() / 2;
        for (int i = 0; i < byteFrameLength - 1; i++) {
            mPoints[i * 4] = mRect.width() * i / (byteFrameLength - 1);
            mPoints[i * 4 + 1] = rectHalfHeight + ((byte) (mBytes[i] + 128)) * rectHalfHeight / 128;
            mPoints[i * 4 + 2] = mRect.width() * (i + 1) / (byteFrameLength - 1);
            mPoints[i * 4 + 3] = rectHalfHeight + ((byte) (mBytes[i + 1] + 128)) * rectHalfHeight / 128;
        }
        canvas.drawLines(mPoints, mForePaint);
    }
}