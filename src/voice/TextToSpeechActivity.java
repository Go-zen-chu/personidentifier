package voice;

import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.Engine;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.go_zen_chu.personidentifier.R;

public class TextToSpeechActivity extends Activity {
	
	private TextToSpeech tts;
    private Button mSpeakButton;
    private EditText mSpeakingEditText;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.text_to_speech_activity_view);
 
        mSpeakButton = (Button) findViewById(R.id.ttsa_SpeakButton);
 
        // button on click event
        mSpeakButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                speakOut();
            }
        });
        
        mSpeakingEditText = (EditText)findViewById(R.id.ttsa_SpeakingEditText);
        
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
			@Override
			public void onInit(int status) {
		        if (status == TextToSpeech.SUCCESS) {
		            int result = tts.setLanguage(Locale.US);
		            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
		                Log.e("TTS", "This Language is not supported");
		            } else {
		                mSpeakButton.setEnabled(true);
		                speakOut();
		            }
		        } else {
		            Log.e("TTS", "Initilization Failed!");
		        }
			}
		});
    }
    private void speakOut() {
        String text = mSpeakingEditText.getText().toString();
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    } 
    
    @Override
    public void onPause(){
       if(tts !=null){
          tts.stop();
          tts.shutdown();
       }
       super.onPause();
    }
    @Override
    public void onDestroy() {
        // Don't forget to shutdown tts!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }
 

}
