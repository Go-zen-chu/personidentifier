package audiolib;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.go_zen_chu.personidentifier.AudioRecordActivity;

import android.R.integer;
import android.R.string;
import android.os.AsyncTask;
import android.os.Debug;
import android.util.Log;

public class WaveFile {
	private WaveHeader mHeader;
	private short mMaxValue;
	private short[] mSampleData;
	private short[] mAveragedSamples;
	private short[] rightChannelSamples;
	private short[] leftChannelSamples;

	public void setHeader(WaveHeader waveHeader) {
		mHeader = waveHeader;
	}

	public WaveHeader getHeader() {
		return mHeader;
	}

	public int getSampleLength() {
		return mSampleData.length;
	}

	public short[] getSamples() {
		return mSampleData;
	}

	public short getMaxValue() {
		return mMaxValue;
	}

	/**
	 * If channel is stereo return samples which are averaged left and right samples
	 */
	public short[] getAveragedSamples() {
		if (mHeader.getNumChannels() == 1)
			return mSampleData;
		if (mAveragedSamples != null)
			return mAveragedSamples;
		mAveragedSamples = new short[mSampleData.length / 2];
		for (int i = 0; i < mAveragedSamples.length; i++) {
			mAveragedSamples[i] = (short) ((mSampleData[2 * i] + mSampleData[2 * i + 1]) / 2);
		}
		return mAveragedSamples;
	}

	public short[] getLeftSamples() {
		getChannelSamples();
		return leftChannelSamples;
	}

	public short[] getRightSamples() {
		getChannelSamples();
		return rightChannelSamples;
	}

	private void getChannelSamples() {
		if (mHeader != null && leftChannelSamples == null
				&& rightChannelSamples == null) {
			int numChannels = mHeader.getNumChannels();
			if (numChannels == 1) {
				leftChannelSamples = mSampleData;
				rightChannelSamples = mSampleData;
				return;
			} else if (numChannels == 2) {
				leftChannelSamples = new short[mSampleData.length / 2];
				rightChannelSamples = new short[mSampleData.length / 2];
				for (int i = 0; i < mSampleData.length / 2; i++) {
					leftChannelSamples[i] = mSampleData[2 * i];
					rightChannelSamples[i] = mSampleData[2 * i + 1];
				}
			}
		}
	}

	public interface AsyncTaskCallback{
		void onPostExecute();
	}
	public class ReadWaveFileAsync extends AsyncTask<String, Integer, Long>{
		AsyncTaskCallback mCallback;
		
		public ReadWaveFileAsync() {
		}
		public AsyncTask<String, Integer, Long> setAsyncTaskCallBack(AsyncTaskCallback callback) {
			mCallback = callback;
			return this;
		}
		
		@Override
		protected Long doInBackground(String... filePathes) {
			DataInputStream dis = null;
			try {
				dis = new DataInputStream(new BufferedInputStream(
						new FileInputStream(filePathes[0])));
				mHeader = new WaveHeader();
				int headerSize = mHeader.read(dis);

				int sampleNum = mHeader.getSamplesNum();
				mSampleData = new short[sampleNum];
				int absValue = 0, maxValue = 0;
				// MONO
				if (mHeader.getBitsPerSample() == 8) {
					byte sampleValue = 0;
					for (int sampleIdx = 0; sampleIdx < sampleNum; sampleIdx++) {
						sampleValue = dis.readByte();
						// considering plus and minus when casting to short
						mSampleData[sampleIdx] = (short) (sampleValue << 8 + sampleValue);
						absValue = Math.abs(mSampleData[sampleIdx]);
						if (absValue > maxValue)
							maxValue = absValue;
					}
					// STEREO
				} else {
					for (int sampleIdx = 0; sampleIdx < sampleNum; sampleIdx++) {
						mSampleData[sampleIdx] = dis.readShort();
						absValue = Math.abs(mSampleData[sampleIdx]);
						if (absValue > maxValue)
							maxValue = absValue;
					}
				}
				mMaxValue = (short) maxValue;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (dis != null)
					try {
						dis.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
			return null;
		}
		@Override
		protected void onPostExecute(Long result) {
			super.onPostExecute(result);
			if(mCallback != null) mCallback.onPostExecute();
		}
	}
	public class WriteWaveFileAsync extends AsyncTask<String, Integer, Long>{
		private String mRawFilePath = null;
		
		public WriteWaveFileAsync(){}
		public WriteWaveFileAsync(String mRawFilePath) {
			this.mRawFilePath = mRawFilePath;
		}
		
		@Override
		protected Long doInBackground(String... filePathes) {
			DataOutputStream dos = null;
			try {
				if (mHeader == null || mHeader.isValid() == false)
					throw new Exception("Prepare header!");
				dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(filePathes[0])));
				mHeader.write(dos);
				if(mRawFilePath != null){
					byte[] beBuffer = new byte[8192];
					byte[] leBuffer = new byte[8192];
					int readSize = 0;
					int totalSize = 0;
					DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(mRawFilePath)));
					while ((readSize = dis.read(beBuffer)) != -1) {
						// raw data is big endian, so it needs to convert to little endian
						if(mHeader.getBitsPerSample() == 16){
							for (int i = 0; i < beBuffer.length / 2; i++) {
								leBuffer[2 * i] = beBuffer[2 * i + 1];
								leBuffer[2 * i + 1] = beBuffer[2 * i];
							}
							dos.write(leBuffer, 0, readSize);
						}
						else
							dos.write(beBuffer, 0, readSize);
						totalSize += readSize;
					}
					Log.i(AudioRecordActivity.LOG_TAG,"r&w " + totalSize + "bytes");
					dis.close();
				}
				else{
					byte[] dataBody = new byte[mSampleData.length * 2];
					for (int i = 0; i < mSampleData.length; i++) {
						dataBody[2 * i] = (byte) (0x0ff & (mSampleData[i] >> 0));
						dataBody[2 * i + 1] = (byte) (0x0ff & (mSampleData[i] >> 8));
					}
					dos.write(dataBody);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (dos != null)
					try {
						dos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Long result) {
			super.onPostExecute(result);
		}
	}
}
