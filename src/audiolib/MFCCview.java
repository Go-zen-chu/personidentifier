package audiolib;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class MFCCview extends LinearLayout {
	int mSplitNum = 0;
	int mCepstraNum = 0;
	Point mDefaultWindowSize;
	ArrayList<double[]> mMFCCList;
	Paint mPaint = new Paint();
	public MFCCview(Context context) {
		super(context);
		initialize();
	}
	public MFCCview(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize();
	}
	void initialize(){
		setWillNotDraw(false); // make this view call onDraw
	}
	
	
	public void setParams(int cepstraNum, int splitNum){
		mSplitNum = splitNum;
		mCepstraNum = cepstraNum;
		mMFCCList = new ArrayList<double[]>(splitNum);
	}
	
	public void insertCepstraData(double[] cepstraData) {
		if(mMFCCList.size() == 12) mMFCCList.remove(0);
		mMFCCList.add(cepstraData);
		invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(mMFCCList == null) return;
		float dx = (float)getWidth() / mSplitNum;
		float dy = (float)getHeight() / mCepstraNum;
		canvas.drawRGB(0x00, 0x00, 0x33);
        for (int frameIdx = 0; frameIdx < mMFCCList.size(); frameIdx++) {
            float x1 = frameIdx * dx;
            float x2 = x1 + dx;
            double[] values = mMFCCList.get(frameIdx);
            
            if (values != null) {
                for (int cepstraIdx = 0; cepstraIdx < values.length; cepstraIdx++) {
                    float y1 = cepstraIdx * dy;
                    float y2 = y1 + dy;
                    mPaint.setColor(valueToColor(values[cepstraIdx]));
                    canvas.drawRect(x1, y1, x2, y2, mPaint);
                }
            }
        }
	}
	
	private int valueToColor(double value) {
		double max = 500, min = -100;
		double valRange = max - min;
		float h = (float)(300 - 300 * (value - min) / valRange);
		int colorCode = Color.HSVToColor(new float[]{h, 1, 1});
		return colorCode;
	}

}
