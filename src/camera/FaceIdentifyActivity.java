package camera;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import android.R.string;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory.Options;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.hardware.Camera.FaceDetectionListener;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Debug;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import androidsurf.IDescriptor;
import androidsurf.IDetector;
import androidsurf.ISURFfactory;
import androidsurf.InterestPoint;
import androidsurf.SURF;

import com.go_zen_chu.personidentifier.AudioRecordActivity;
import com.go_zen_chu.personidentifier.R;

public class FaceIdentifyActivity extends Activity {
	// Menuの設定
    private static final String MENU_TRAIN_MODEL = "Train Model";
    private static final String MENU_CALC_SURF = "Calc SURF";
    private static final String MENU_SURF_MATCHING = "SURF Matching";
    
    private HashMap<String, OnMenuItemClickListener> mMenuHashMap
		= new HashMap<String, MenuItem.OnMenuItemClickListener>(){{
			put(MENU_TRAIN_MODEL, new OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					trainModel();
					return false;
				}
			});
			put(MENU_CALC_SURF, new OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					calculateEachSURF();
					return false;
				}
			});
			put(MENU_SURF_MATCHING, new OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					if(mFeaturePersonNameDict != null){
						mIsIdentifyingFace = true;
					}
					else {
						
					}
					return false;
				}
			});
		}};
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		for(String key : mMenuHashMap.keySet()){
    		MenuItem menuItem = menu.add(key);
    		menuItem.setOnMenuItemClickListener(mMenuHashMap.get(key));
    		menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
    	}
		return true;
	}	
	
	
	private static final String PERSON_DIR = "Person/";
	private static final String ARFF_DIR = "ARFF/";
	private static final String MODEL_DIR = "Model/";
	
	HashMap<ArrayList<InterestPoint>, String> mFeaturePersonNameDict;
	boolean mIsIdentifyingFace = false;
	
	private void calculateEachSURF() {
		Thread calcSURFThread = new Thread(new Runnable() {
			@Override
			public void run() {
				String faceDataPath = AudioRecordActivity.PROJECT_PATH + PERSON_DIR;
				File faceRootDir = new File(faceDataPath);
				
				mFeaturePersonNameDict  = new HashMap<ArrayList<InterestPoint>, String>();
				File[] personalDirs = faceRootDir.listFiles();
				for(File personalDir : personalDirs){
					File[] faceImgs = personalDir.listFiles();
					String personName = personalDir.getName();
					Bitmap bmp = BitmapFactory.decodeFile(faceImgs[0].getAbsolutePath());
					mFeaturePersonNameDict.put(calcSURF(bmp), personName); // SURF特徴と人の名前を辞書に格納する（時間がかかる）
				}
			}
		});
		calcSURFThread.start();
	}

	// SURF特徴を学習する方法（時間かかりそう）
	private void trainModel() {
		Thread trainingThread = new Thread(new Runnable() {
			@Override
			public void run() {
				String faceDataPath = AudioRecordActivity.PROJECT_PATH + PERSON_DIR;
				File faceRootDir = new File(faceDataPath);
				String arffPath = AudioRecordActivity.PROJECT_PATH + ARFF_DIR;
				File arffRootDir = new File(arffPath);
				
				File[] personalDirs = faceRootDir.listFiles();
				for(File personalDir : personalDirs){
					File[] faceImgs = personalDir.listFiles();
					String personName = personalDir.getName() + "/";
					for(File faceImg : faceImgs){
						String fileName = faceImg.getName();
						int dotPos = fileName.lastIndexOf('.');
						String baseName = fileName.substring(dotPos + 1);
						File featureArffFile = new File(arffPath + personName + baseName + ".arff");
						if(featureArffFile.exists() == false){
							Bitmap bmp = BitmapFactory.decodeFile(faceImg.getAbsolutePath());
							ArrayList<InterestPoint> ip = calcSURF(bmp);
							
						}
					}
				}

			}
		});
		trainingThread.start();		
	}
	
    FaceOverlayView mFaceOverlayView;
	private Camera mCamera;
	private int mDegree = 90;
	private long mPrevCaptureTime = 0;
	private static final long CAPTURE_INTERVAL = 100;
    private Rect[] mPrevFaceRects;
    private Camera.PreviewCallback mPreviewCallback = new Camera.PreviewCallback() {
		@Override
		public void onPreviewFrame(byte[] data, Camera camera) {
			// 基本的な条件分岐
			long currentTime = new Date().getTime();
			if(mCamera == null || mFaceOverlayView.mUnRotatedRects == null || currentTime - mPrevCaptureTime < CAPTURE_INTERVAL) return;
			RectF[] faceRectFs = mFaceOverlayView.mUnRotatedRects;
			Camera.Parameters cameraParams = mCamera.getParameters();
			int viewWidth = mFaceOverlayView.getWidth(); // 画面の幅
			int viewHeight = mFaceOverlayView.getHeight(); // 画面の高さ
			Rect[] rects = new Rect[faceRectFs.length];
			Size prevSize = cameraParams.getPreviewSize();
			// previewのサイズとViewのサイズが異なるため、Viewサイズに合わせたRectのサイズをBitmapのサイズに変更する
			float widthRacio = (float)prevSize.width / viewHeight; // 縦向きのため
			float heightRacio = (float)prevSize.height / viewWidth; // 縦向きのため
			for (int i = 0; i < faceRectFs.length; i++) {
				rects[i] =  new Rect((int)(faceRectFs[i].left * widthRacio), (int)(faceRectFs[i].top * heightRacio), 
									(int)(faceRectFs[i].right * widthRacio), (int)(faceRectFs[i].bottom * heightRacio));
			}
			// 変化がない場合はデータを保存しない
			if(mPrevFaceRects != null && isSameRect(rects[0], mPrevFaceRects[0])){
				Log.i(AudioRecordActivity.LOG_TAG, "same rect");
				return;
			}
			if(mPrevFaceRects != null) Log.i(AudioRecordActivity.LOG_TAG, rects[0] + ", " + mPrevFaceRects[0]);
			mPrevCaptureTime = currentTime;
			mPrevFaceRects = rects;
			IdentifyFaces sf = new IdentifyFaces(data, rects, cameraParams.getPreviewFormat(), prevSize);
			Thread saveFaceBmpThread = new Thread(sf);
			saveFaceBmpThread.start();
		}
	};
	private boolean isSameRect(Rect r1, Rect r2) {
		int OFFSET = 2;
		Rect largerRect = new Rect(r1.left - OFFSET, r1.top - OFFSET, r1.right + OFFSET, r1.bottom + OFFSET);
		return largerRect.contains(r2);
//		return r1.left == r2.left && r1.top == r2.top && r1.right == r2.right && r1.bottom == r2.bottom;
	}
	
	TextToSpeech mTTS;
	
	private class IdentifyFaces implements Runnable{
		byte[] byteData;
		Rect[] faceRects;
		int previewFormat;
		Size prevSize;
		public IdentifyFaces(byte[] byteData, Rect[] faceRects, int previewFormat,Size prevSize){
			this.byteData = byteData; this.faceRects = faceRects; this.previewFormat = previewFormat; this.prevSize = prevSize;
		}
		
		@Override
		public void run() {
			if(mCamera == null) return;
			BitmapFactory.Options bfo = new BitmapFactory.Options();
			bfo.inPreferredConfig = Config.RGB_565;
			ByteArrayOutputStream baos;
			mCamera.addCallbackBuffer(byteData);
			try {
				// prevSize.width = 640, prevSize.height = 480 on galaxy s2
				YuvImage img = new YuvImage(byteData, previewFormat, prevSize.width, prevSize.height, null);
				baos = new ByteArrayOutputStream();
				img.compressToJpeg(faceRects[0], 90, baos);
				byte[] jpgData = baos.toByteArray();
				Bitmap bmp = BitmapFactory.decodeByteArray(jpgData, 0, jpgData.length, bfo);
				ArrayList<InterestPoint> capturedIPList = calcSURF(bmp);
				String matchedName = null;
				for(ArrayList<InterestPoint> ipList : mFeaturePersonNameDict.keySet()){
					int matchedNum = 0;
					int ipLength = ipList.size();
					for(InterestPoint ip : ipList){
						for(InterestPoint capIP : capturedIPList){
							if(isInterestPointMatches(ip, capIP)) matchedNum++;
						}
					}
					double matchRate = matchedNum / (double)ipLength;
					Log.i(AudioRecordActivity.LOG_TAG, mFeaturePersonNameDict.get(ipList) + " " +  matchRate);
					if(matchRate > 0.6){
						matchedName = mFeaturePersonNameDict.get(ipList);
						break;
					}
				}
				if(matchedName != null){
					final String finalMatchedName = matchedName;
					mTTS = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener(){
						@Override
						public void onInit(int status) {
							if (status == TextToSpeech.SUCCESS) {
					            int result = mTTS.setLanguage(Locale.US);
					            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
					                Log.e("TTS", "This Language is not supported");
					            } else {
					                speakName(finalMatchedName);
					            }
					        }
						}
					});
				}
			 } catch (Exception e) {
				 e.printStackTrace();
			 }
		}
	}
	private void speakName(String name) {
		if(mTTS != null){
			mTTS.speak("Hello " + name, TextToSpeech.QUEUE_FLUSH, null);
		}
	}
	 
	private boolean isInterestPointMatches(InterestPoint ip1, InterestPoint ip2) {
		double squareSum = 0;
		float[] f1 = ip1.getDescriptorOfTheInterestPoint();
		float[] f2 = ip2.getDescriptorOfTheInterestPoint();
		for(int fIdx = 0; fIdx < f1.length; fIdx++){
			squareSum += (f1[fIdx] - f2[fIdx]) * (f1[fIdx] - f2[fIdx]);
		}
		Log.i(AudioRecordActivity.LOG_TAG, " " + squareSum);
		return squareSum < 1; // 閾値よりも低かったらマッチしている
	}
    
	
	private SurfaceHolder.Callback mSurfaceHandler = new SurfaceHolder.Callback() {
        public void surfaceCreated(SurfaceHolder holder) {
            mCamera = Camera.open();
            mCamera.setDisplayOrientation(mDegree);
            Camera.Parameters params = mCamera.getParameters();
            params.setRotation(mDegree);
//          params.setPictureSize(800, 480);
            mCamera.setParameters(params);
            mCamera.setFaceDetectionListener(mFaceDetectHandler);
            // 30fpsのデータが取れる。顔認識する度に、顔の座標を更新して、写真を保存（画質は悪い）
            mCamera.setPreviewCallback(mPreviewCallback);
            try {
                mCamera.setPreviewDisplay(holder);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        public void surfaceDestroyed(SurfaceHolder holder) {
        	mCamera.stopFaceDetection();
        	mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
//            Camera.Parameters parameters = mCamera.getParameters();
//            parameters.setPreviewSize(width, height);
//            mCamera.setParameters(parameters);
            mCamera.startPreview();
        }
    };
	private FaceDetectionListener mFaceDetectHandler = new FaceDetectionListener() {
		@Override
		public void onFaceDetection(Face[] faces, Camera camera) {
			if(mFaceOverlayView == null) return;
			mFaceOverlayView.setFaces(faces); // 顔の座標の設定
		}
	};
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		//getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.face_detect_activity_view);
		
		mFaceOverlayView = new FaceOverlayView(this);
		// 動的にレイアウトを作成する
		addContentView(mFaceOverlayView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		
		SurfaceView sv = (SurfaceView)findViewById(R.id.face_detect_activity_surface_view);
		SurfaceHolder holder = sv.getHolder();
		holder.addCallback(mSurfaceHandler);
    }
    
    static final float threshold = 800;
    static final float balanceValue = (float) 0.9;
    static final int octaves = 5;
    // SURFを計算して、全ピクセルの特徴を返す
	public ArrayList<InterestPoint> calcSURF(Bitmap bmp) {
		Log.i(AudioRecordActivity.LOG_TAG, "Start calc SURF " + AudioRecordActivity.DATE_FORMAT.format(new Date()));
		ISURFfactory mySURF = SURF.createInstance(bmp, balanceValue, threshold, octaves, bmp);
		Log.i(AudioRecordActivity.LOG_TAG, "1 " + AudioRecordActivity.DATE_FORMAT.format(new Date()));
        IDetector detector = mySURF.createDetector();
        Log.i(AudioRecordActivity.LOG_TAG, "2 " + AudioRecordActivity.DATE_FORMAT.format(new Date()));
        ArrayList<InterestPoint> interest_points = detector.generateInterestPoints(); // 18sec 
        Log.i(AudioRecordActivity.LOG_TAG, "3 " + AudioRecordActivity.DATE_FORMAT.format(new Date()));
        IDescriptor descriptor = mySURF.createDescriptor(interest_points);
        Log.i(AudioRecordActivity.LOG_TAG, "4 " + AudioRecordActivity.DATE_FORMAT.format(new Date()));
        descriptor.generateAllDescriptors(); // 9sec
		Log.i(AudioRecordActivity.LOG_TAG, "Finish calc SURF " + AudioRecordActivity.DATE_FORMAT.format(new Date()));
        return interest_points;
	}

	
}
