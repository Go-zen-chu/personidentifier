package camera;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.hardware.Camera.FaceDetectionListener;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;

import com.go_zen_chu.personidentifier.AudioRecordActivity;
import com.go_zen_chu.personidentifier.R;

public class FaceDetectActivity extends Activity {
	// Menuの設定
    private static final String MENU_TAKE_FACE_PHOTO = "Take Photo";
    private HashMap<String, OnMenuItemClickListener> mMenuHashMap
		= new HashMap<String, MenuItem.OnMenuItemClickListener>(){{
			put(MENU_TAKE_FACE_PHOTO, new OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					takeFacePhoto();
					return false;
				}
			});
		}};
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		for(String key : mMenuHashMap.keySet()){
    		MenuItem menuItem = menu.add(key);
    		menuItem.setOnMenuItemClickListener(mMenuHashMap.get(key));
    	}
		return super.onCreateOptionsMenu(menu);
	}	
		
	
    public static final String CAMERA_DIR = "Camera/";
	private Camera mCamera;
	private int mDegree = 90;
    private FaceOverlayView mFaceOverlayView;
	boolean mIsTakePhotoReady = true;
	
	// saving faces automatically
	private long mPrevCaptureTime = 0;
	private static final long CAPTURE_INTERVAL = 100;
    private Rect[] mPrevFaceRects;
    private Camera.PreviewCallback mPreviewCallback = new Camera.PreviewCallback() {
		@Override
		public void onPreviewFrame(byte[] data, Camera camera) {
			// 基本的な条件分岐
			long currentTime = new Date().getTime();
			if(mCamera == null || mFaceOverlayView.mUnRotatedRects == null || currentTime - mPrevCaptureTime < CAPTURE_INTERVAL) return;
			RectF[] faceRectFs = mFaceOverlayView.mUnRotatedRects;
			Camera.Parameters cameraParams = mCamera.getParameters();
			int viewWidth = mFaceOverlayView.getWidth(); // 画面の幅
			int viewHeight = mFaceOverlayView.getHeight(); // 画面の高さ
			Rect[] rects = new Rect[faceRectFs.length];
			Size prevSize = cameraParams.getPreviewSize();
			// previewのサイズとViewのサイズが異なるため、Viewサイズに合わせたRectのサイズをBitmapのサイズに変更する
			float widthRacio = (float)prevSize.width / viewHeight; // 縦向きのため
			float heightRacio = (float)prevSize.height / viewWidth; // 縦向きのため
			for (int i = 0; i < faceRectFs.length; i++) {
				rects[i] =  new Rect((int)(faceRectFs[i].left * widthRacio), (int)(faceRectFs[i].top * heightRacio), 
									(int)(faceRectFs[i].right * widthRacio), (int)(faceRectFs[i].bottom * heightRacio));
			}
			// 変化がない場合はデータを保存しない
			if(mPrevFaceRects != null && isSameRect(rects[0], mPrevFaceRects[0])){
				Log.i(AudioRecordActivity.LOG_TAG, "same rect");
				return;
			}
			if(mPrevFaceRects != null) Log.i(AudioRecordActivity.LOG_TAG, rects[0] + ", " + mPrevFaceRects[0]);
			mPrevCaptureTime = currentTime;
			mPrevFaceRects = rects;
			SaveFaces sf = new SaveFaces(data, rects, cameraParams.getPreviewFormat(), prevSize);
			Thread saveFaceBmpThread = new Thread(sf);
			saveFaceBmpThread.start();
		}
	};
	private boolean isSameRect(Rect r1, Rect r2) {
		int OFFSET = 2;
		Rect largerRect = new Rect(r1.left - OFFSET, r1.top - OFFSET, r1.right + OFFSET, r1.bottom + OFFSET);
		return largerRect.contains(r2);
//		return r1.left == r2.left && r1.top == r2.top && r1.right == r2.right && r1.bottom == r2.bottom;
	}
	
	private class SaveFaces implements Runnable{
		byte[] byteData;
		Rect[] faceRects;
		int previewFormat;
		Size prevSize;
		
		public SaveFaces(byte[] byteData, Rect[] faceRects, int previewFormat,Size prevSize){
			this.byteData = byteData; this.faceRects = faceRects; this.previewFormat = previewFormat; this.prevSize = prevSize;
		}
		
		@Override
		public void run() {
			if(mCamera == null) return;
			String dateStr = AudioRecordActivity.DATE_FORMAT.format(new Date());
			String savingDir = AudioRecordActivity.PROJECT_PATH + CAMERA_DIR;
			new File(savingDir).mkdirs();
			FileOutputStream fos;
			mCamera.addCallbackBuffer(byteData);
			try {
				// prevSize.width = 640, prevSize.height = 480 on galaxy s2
				YuvImage img = new YuvImage(byteData, previewFormat, prevSize.width, prevSize.height, null);
				fos = new FileOutputStream(new File(savingDir, dateStr + ".jpg"));
				img.compressToJpeg(faceRects[0], 90, fos);
				for(int i = 1; i < faceRects.length; i++){
					fos = new FileOutputStream(new File(savingDir, dateStr + "_" + i + ".jpg"));
					img.compressToJpeg(faceRects[i], 90, fos);
				}
			 } catch (Exception e) {
				 e.printStackTrace();
			 }
		}
		
	}
    
	private SurfaceHolder.Callback mSurfaceHandler = new SurfaceHolder.Callback() {
	        public void surfaceCreated(SurfaceHolder holder) {
                mCamera = Camera.open();
                mCamera.setDisplayOrientation(mDegree);
                Camera.Parameters params = mCamera.getParameters();
                params.setRotation(mDegree);
//              params.setPictureSize(800, 480);
                mCamera.setParameters(params);
                mCamera.setFaceDetectionListener(mFaceDetectHandler);
                // 30fpsのデータが取れる。顔認識する度に、顔の座標を更新して、写真を保存（画質は悪い）
                mCamera.setPreviewCallback(mPreviewCallback);
                try {
                    mCamera.setPreviewDisplay(holder);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            public void surfaceDestroyed(SurfaceHolder holder) {
            	mCamera.stopFaceDetection();
            	mCamera.stopPreview();
                mCamera.release();
                mCamera = null;
            }
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
	            Camera.Parameters parameters = mCamera.getParameters();
	            parameters.setPreviewSize(width, height);
	            mCamera.setParameters(parameters);
	            mCamera.startPreview();
	        }
	    };
	    
//	    必要になったら使う
//	private AutoFocusCallback mAutoFocusHandler = new AutoFocusCallback() {
//		@Override
//		public void onAutoFocus(boolean success, Camera camera) {
//			if(success == false) return;
//			
//		}
//	};
	    
	private FaceDetectionListener mFaceDetectHandler = new FaceDetectionListener() {
		@Override
		public void onFaceDetection(Face[] faces, Camera camera) {
			if(mFaceOverlayView == null) return;
			mFaceOverlayView.setFaces(faces); // 顔の座標の設定
		}
	};
    
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.face_detect_activity_view);
		
		mFaceOverlayView = new FaceOverlayView(this);
		// 動的にレイアウトを作成する
		addContentView(mFaceOverlayView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		
		SurfaceView sv = (SurfaceView)findViewById(R.id.face_detect_activity_surface_view);
		SurfaceHolder holder = sv.getHolder();
		holder.addCallback(mSurfaceHandler);
		
	}
		
	// take photo of faces
	private void takeFacePhoto(){
		if(mCamera == null || mFaceOverlayView == null || mFaceOverlayView.mUnRotatedRects == null) return;
		mCamera.takePicture(null, null, new PictureCallback() {
			@Override
			public void onPictureTaken(byte[] data, Camera camera) {
				// 50ms くらいでここの箇所は終わる
				String dateStr = AudioRecordActivity.DATE_FORMAT.format(new Date());
				String savingDirPath = AudioRecordActivity.PROJECT_PATH + CAMERA_DIR + dateStr;
				File dir = new File(savingDirPath);
				dir.mkdirs();
				// takePictureでは高解像度で写真が撮れるため、処理に時間がかかる
				SaveFacePhotoAsyncTask task = new SaveFacePhotoAsyncTask(mFaceOverlayView.mUnRotatedRects, camera.getParameters().getPictureSize(), data, savingDirPath);
				task.execute(dateStr);
				camera.startPreview();
				mIsTakePhotoReady = true;
			}
		});
		mIsTakePhotoReady = false;
	}
	// saving photo of the faces async
	private class SaveFacePhotoAsyncTask extends AsyncTask<String, Integer, Long>{
		RectF[] mFaceRects;
		Size mPicSize;
		byte[] mData;
		String mSavingDirPath;
		
		public SaveFacePhotoAsyncTask(RectF[] faceRects, Size picSize, byte[] data, String savingDirPath){
			mFaceRects = faceRects; mPicSize = picSize; mData = data; mSavingDirPath = savingDirPath;
		}
		@Override
		protected Long doInBackground(String... dateStr) {
			FileOutputStream fos = null;
			try{
				final Rect[] rects = new Rect[mFaceRects.length];
				final int[][] facePixels = new int[rects.length][];
				// 写真のサイズとViewのサイズが異なるため、Viewサイズに合わせたRectのサイズをBitmapのサイズに変更する
				final float widthRacio = (float)mPicSize.width / mFaceOverlayView.getHeight(); // 縦向きのため
				final float heightRacio = (float)mPicSize.height / mFaceOverlayView.getWidth() ; // 縦向きのため
				Rect newRect = null;
				for (int i = 0; i < mFaceRects.length; i++) {
					newRect = new Rect((int)(mFaceRects[i].left * widthRacio), (int)(mFaceRects[i].top * heightRacio), (int)(mFaceRects[i].right * widthRacio), (int)(mFaceRects[i].bottom * heightRacio));
					rects[i] = newRect;
					facePixels[i] = new int[newRect.width() * newRect.height()];
				}
				Bitmap[] faceBmps = new Bitmap[rects.length];
				Bitmap bmp = BitmapFactory.decodeByteArray(mData, 0, mData.length);
				for(int i = 0; i < rects.length; i++){
					bmp.getPixels(facePixels[i], 0, rects[i].width(), rects[i].left, rects[i].top, rects[i].width(), rects[i].height());
					Bitmap cropBmp = Bitmap.createBitmap(facePixels[i], 0, rects[i].width(), rects[i].width(), rects[i].height(), Config.ARGB_4444);
					faceBmps[i] = cropBmp;
				}
				bmp.recycle();
				fos = new FileOutputStream(new File(mSavingDirPath, dateStr[0] + ".png"));
				faceBmps[0].compress(Bitmap.CompressFormat.PNG, 100, fos);
				for(int i = 1; i < faceBmps.length; i++){
					fos = new FileOutputStream(new File(mSavingDirPath, dateStr[0] + "_" + i + ".png"));
					faceBmps[i].compress(Bitmap.CompressFormat.PNG, 100, fos);
				}
			}catch(FileNotFoundException e){
				e.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
			}
			finally{
				if(fos != null) try{fos.close(); } catch (Exception e) { e.printStackTrace(); }
			}
			return null;
		}		
	}
	
}
