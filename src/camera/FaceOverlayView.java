package camera;

import com.go_zen_chu.personidentifier.AudioRecordActivity;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.hardware.Camera.Face;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class FaceOverlayView extends View {
    private Paint mPaint;
    public RectF[] mRotatedRects; // 描画用に回転させる（横向きの場合はmUnRotatedRectsと同じになる）
    public RectF[] mUnRotatedRects; // 横向き時の左上を原点とした矩形
    private Matrix mMat;
    private static final int ROTATION = 90; //縦向きに対応
 
    public FaceOverlayView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialize();
    }
 
    public FaceOverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }
 
    public FaceOverlayView(Context context) {
        super(context);
        initialize();
    }
     
    private void initialize() {
        // 塗りつぶしの設定
        mPaint = new Paint();
        mPaint.setColor(Color.MAGENTA);
        mPaint.setAlpha(128);
        mPaint.setStyle(Paint.Style.FILL);
        mMat = new Matrix();
    }
 
    public void setFaces(Face[] faces) {
        if(faces == null || faces.length == 0) {
        	mRotatedRects = null;
        }else{
        	mRotatedRects = new RectF[faces.length];
        	mUnRotatedRects = new RectF[faces.length];
        	int w = getWidth(); int h = getHeight();
        	for (int i =0; i < faces.length; i++) {
//                if (faces[i] == null) continue;
                // カメラドライバ側が -1000 〜 1000 の範囲で処理しているので、実際の画面上の座標にするには以下のように Matrix を使って座標の変換をかける必要がある
                mMat.reset();
                mMat.postRotate(ROTATION); //カメラの回転に合わせる
                mMat.postScale(w / 2000f, h / 2000f); // サイズの縮小
                mMat.postTranslate(w / 2f, h / 2f); // 移動
                mRotatedRects[i] = new RectF(faces[i].rect);
                mMat.mapRect(mRotatedRects[i]);
                //　縦向き専用のコード
                mMat.reset();
                mMat.postScale(h/2000f, w/2000f);
                mMat.postTranslate(h/2f, w/2f);
                mUnRotatedRects[i] = new RectF(faces[i].rect);
                mMat.mapRect(mUnRotatedRects[i]);
            }
        }
        invalidate();
    }
    
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(mRotatedRects == null){
        	canvas.drawColor(Color.TRANSPARENT);
            return;
        }
        for(RectF rect: mRotatedRects){
        	canvas.drawRect(rect, mPaint);
        }
    }
    
}